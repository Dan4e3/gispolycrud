"""create gis_polygons table

Revision ID: 847e58126c4a
Revises: 
Create Date: 2021-09-07 21:42:48.846332

"""
from alembic import op
import sqlalchemy as sa
from models.gis_polygon import GisPolygon
from sqlalchemy import orm


# revision identifiers, used by Alembic.
revision = '847e58126c4a'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    bind = op.get_bind()
    GisPolygon.__table__.create(bind)

def downgrade():
    op.drop_table(GisPolygon.__tablename__)
