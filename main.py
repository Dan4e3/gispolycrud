from fastapi import FastAPI
from routers import polygons
from db import database

app = FastAPI()
app.include_router(polygons.router)

@app.on_event("startup")
async def startup():
    await database.connect()

@app.on_event("shutdown")
async def shutdown():
    await database.disconnect()

@app.get("/")
async def root():
    return {"message": "Testing entry point"}
