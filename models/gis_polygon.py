from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, JSON, DateTime, func
from geoalchemy2 import Geometry

Base = declarative_base()
class GisPolygon(Base):
    __tablename__ = "gis_polygon"
    _created = Column(DateTime(timezone=False), default=func.now())
    _updated = Column(DateTime(timezone=False))
    id = Column(Integer, primary_key=True)
    class_id = Column(Integer)
    name = Column(String)
    props = Column(JSON)
    geom = Column(Geometry(geometry_type='POLYGON', srid=4326))


    
    
