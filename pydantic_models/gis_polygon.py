from pydantic import BaseModel, Json
from typing import Tuple, List, Optional

class GisPolygon(BaseModel):
    class_id: int
    name: str
    props: Json
    geom: List[Tuple[int, int]]
    projection: Optional[str]

class GisPolygonUpd(BaseModel):
    class_id: Optional[int]
    name: Optional[str]
    props: Optional[Json]
    geom: Optional[List[Tuple[int, int]]]
