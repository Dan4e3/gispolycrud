from models.gis_polygon import GisPolygon
from utils import get_wkt_with_projection
from db import database, DEFAULT_CRS
from geoalchemy2 import functions
from sqlalchemy.sql import select, update
from sqlalchemy import func

class GisPolygonsRepository():
    def __init__(self):
        pass

    async def get_all(self, projection):
        if projection is not None:
            geom_query = GisPolygon.geom
        else:
            geom_query = functions.ST_AsText(GisPolygon.geom).label("geom")        
        query = select(GisPolygon.id,
                       GisPolygon.class_id,
                       GisPolygon.name,
                       GisPolygon.props,
                       geom_query)
        return await database.fetch_all(query)

    async def get_one(self, id, projection):
        geom_query = None
        if projection is not None:
            geom_query = GisPolygon.geom
        else:
            geom_query = functions.ST_AsText(GisPolygon.geom).label("geom")
        query = select(GisPolygon.id,
                       GisPolygon.class_id,
                       GisPolygon.name,
                       GisPolygon.props,
                       geom_query) \
                .where(GisPolygon.id == id)
        return await database.fetch_one(query)

    async def create(self, gis_poly):
        polygon = ("{} {}".format(x[0], x[1]) for x in gis_poly.geom)
        polygon_str = ",".join(polygon)
        geom_str = "POLYGON(({}))".format(polygon_str)
        if gis_poly.projection is not None:
            geom_str = get_wkt_with_projection(geom_str, gis_poly.projection)
        query = GisPolygon.__table__ \
                .insert() \
                .values(_created = func.now(),
                        _updated = func.now(),
                        class_id = gis_poly.class_id,
                        name = gis_poly.name,
                        props = gis_poly.props,
                        geom = "SRID={};{}".format(DEFAULT_CRS, geom_str))
        return await database.execute(query)

    async def delete(self, id):
        query = GisPolygon.__table__.delete().where(GisPolygon.id == id)
        gis_poly = await database.fetch_one(
            select(GisPolygon.id).where(GisPolygon.id == id))
        await database.execute(query)
        return gis_poly

    async def update(self, id, gis_poly):
        query = update(GisPolygon) \
                .where(GisPolygon.id == id) \
                .values(_updated = func.now(),
                        **gis_poly.dict(exclude_unset=True))
        await database.execute(query)
        return await database.fetch_one(
            select(GisPolygon.id,
                   GisPolygon.class_id,
                   GisPolygon.name,
                   GisPolygon.props,
                   functions.ST_AsText(GisPolygon.geom).label("geom")) \
            .where(GisPolygon.id == id))
                
