from fastapi import APIRouter
from repositiories.gis_polygons_repo import GisPolygonsRepository
from pydantic_models.gis_polygon import GisPolygon as GisPolygonPydantic
from pydantic_models.gis_polygon import GisPolygonUpd
from db import db_engine
from typing import Optional
from utils import project_gispolygons, transform_polygon

gis_poly_repo = GisPolygonsRepository()

router = APIRouter(
        prefix="/polygons",
        tags=["polygons"]
    )

@router.get("/")
async def get_all_items(projection: Optional[str] = None):
    gis_polygons = await gis_poly_repo.get_all(projection)
    if projection is not None:
        gis_polygons = project_gispolygons(gis_polygons, projection)
    return gis_polygons

@router.get("/{id}")
async def get_one_item(id: int, projection: Optional[str] = None):
    gis_polygon = await gis_poly_repo.get_one(id, projection)
    if gis_polygon is not None and projection is not None:
        gis_polygon = dict(gis_polygon.items())
        gis_polygon["geom"] = transform_polygon(gis_polygon["geom"], projection)    
    return gis_polygon

@router.post("/")
async def create(gis_poly: GisPolygonPydantic):
    res = await gis_poly_repo.create(gis_poly)
    return {**gis_poly.dict(), "id": res}

@router.delete("/{id}")
async def delete(id: int):
    return await gis_poly_repo.delete(id)

@router.put("/{id}")
async def update(id: int, gis_poly: GisPolygonUpd):
    return await gis_poly_repo.update(id, gis_poly)
    
    
