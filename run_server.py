import uvicorn

if __name__ == "__main__":
    uvicorn.run("main:app", host="localhost", port=9000, log_level="info", debug=True)
