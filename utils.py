import pyproj
from shapely.geometry import Polygon
from shapely.ops import transform
from shapely import wkb, wkt
from geoalchemy2.shape import to_shape 
from db import DEFAULT_CRS

def project_gispolygons(gispolygons_list, target_proj, base_proj = DEFAULT_CRS):
    res = []
    for gis_polygon in gispolygons_list:
        gis_polygon = dict(gis_polygon.items())
        gis_polygon["geom"] = transform_polygon(gis_polygon["geom"], target_proj, base_proj)
        res.append(gis_polygon)
    return res

def transform_polygon(geometry, target_proj, base_proj = DEFAULT_CRS):
    polygon = to_shape(geometry)
    base_CRS = pyproj.CRS("EPSG:{}".format(base_proj))
    target_CRS = pyproj.CRS("EPSG:{}".format(target_proj))
    transformer = pyproj.Transformer.from_crs(base_CRS, target_CRS, always_xy=True).transform
    transformed_polygon = transform(transformer, polygon)
    return transformed_polygon.to_wkt()

def get_wkt_with_projection(target_wkt, input_proj):
    p_wkt = wkt.loads(target_wkt)
    base_CRS = pyproj.CRS("EPSG:{}".format(input_proj))
    target_CRS = pyproj.CRS("EPSG:{}".format(DEFAULT_CRS))
    transformer = pyproj.Transformer.from_crs(base_CRS, target_CRS, always_xy=True).transform
    transformed_polygon = transform(transformer, p_wkt)
    return transformed_polygon.to_wkt()    
